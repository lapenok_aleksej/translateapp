package ru.ifmo.lapenok.translate.database;

/**
 * Created by Алексей on 25.03.2017.
 */

public class Translate {//class to contains translations

    private int id;
    private String firstCode;
    private String secondCode;
    private String input;
    private String output;
    private int favourite;

    public Translate() {
    }

    public Translate(int id, String firstCode, String secondCode, String input, String output, int favourite) {
        this.id = id;
        this.firstCode = firstCode;
        this.secondCode = secondCode;
        this.input = input;
        this.output = output;
        this.favourite = favourite;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstCode() {
        return firstCode;
    }

    public void setFirstCode(String firstCode) {
        this.firstCode = firstCode;
    }

    public String getSecondCode() {
        return secondCode;
    }

    public void setSecondCode(String secondCode) {
        this.secondCode = secondCode;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public int getFavourite() {
        return favourite;
    }

    public void setFavourite(int favourite) {
        this.favourite = favourite;
    }

    @Override
    public String toString() {
        return "Translate{" +
                "firstCode='" + firstCode + '\'' +
                ", secondCode='" + secondCode + '\'' +
                ", input='" + input + '\'' +
                ", output='" + output + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object other1) {
        if (other1 instanceof Translate) {
            Translate other = (Translate) other1;
            return id == other.id ||
                    (firstCode.equals(other.firstCode) && secondCode.equals(other.secondCode) && input.equals(other.input) && output.equals(other.output));
        }
        return false;
    }

    @Override
    public int hashCode() {
        return firstCode.hashCode() * 1000 + secondCode.hashCode() * 100 + input.hashCode() * 10 + output.hashCode();
    }
}
