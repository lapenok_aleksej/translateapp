package ru.ifmo.lapenok.translate.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import ru.ifmo.lapenok.translate.R;
import ru.ifmo.lapenok.translate.dialogs.DeleteAllDialogFragment;
import ru.ifmo.lapenok.translate.loaders.DeleteAllTranslatesLoader;
import ru.ifmo.lapenok.translate.loaders.callback.CallBack;
import ru.ifmo.lapenok.translate.loaders.callback.ISaveData;

/**
 * Created by Алексей on 07.04.2017.
 */

public class HistoryActivity extends AppCompatActivity {

    private static final String TAG = "HistoryActivity";

    public static final int FRAGMENT_HISTORY = 0;
    public static final int FRAGMENT_FAVORITE = 1;

    private static final int DELETE_ALL_TRANSLATES = 413;

    HistoryPagerAdapter historyPagerAdapter;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_history);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);//set toolbar
        setSupportActionBar(toolbar);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);//setting of viewPager
        historyPagerAdapter = new HistoryPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(historyPagerAdapter);
        viewPager.setCurrentItem(getIntent().getFlags());
        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener());

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);//setting of tabLayout
        tabLayout.setupWithViewPager(viewPager);

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        findViewById(R.id.clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment dialog = new DeleteAllDialogFragment();//confirm to delete all translations
                dialog.show(getSupportFragmentManager(), "");
            }
        });

        overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
    }

    public void updateFragment(int position) {
        historyPagerAdapter.getFragment(position).update();//update fragment if something was changed
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {//if user want to delete all translates
        TranslateActivity.translates = new ArrayList<>();
        DeleteAllTranslatesLoader loader = new DeleteAllTranslatesLoader(this);
        getSupportLoaderManager().restartLoader(DELETE_ALL_TRANSLATES, null, new CallBack<>(new FinishDelete(), loader, DELETE_ALL_TRANSLATES));
        historyPagerAdapter.getFragment(FRAGMENT_HISTORY).update();
        historyPagerAdapter.getFragment(FRAGMENT_FAVORITE).update();
    }


    @Override
    public void onBackPressed() {
        finish();
        startActivity(new Intent(HistoryActivity.this, TranslateActivity.class));//go back to translate activity
    }

    private class FinishDelete implements ISaveData<Void> {
        @Override
        public void saveData(Void data, int id) {
            getSupportLoaderManager().destroyLoader(id);
        }
    }

    private class HistoryPagerAdapter extends FragmentPagerAdapter {

        private static final int COUNT = 2;

        private final Map<Integer, HistoryFragment> fragments = new TreeMap<>();

        HistoryPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public int getCount() {
            return COUNT;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case FRAGMENT_HISTORY:
                case FRAGMENT_FAVORITE:
                    HistoryFragment historyFragment = new HistoryFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt(HistoryFragment.TYPE, position);//send number of position
                    historyFragment.setArguments(bundle);
                    return historyFragment;
                default:
                    return null;
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case FRAGMENT_FAVORITE:
                    return getResources().getText(R.string.favorite).toString().toUpperCase();
                case FRAGMENT_HISTORY:
                    return getResources().getText(R.string.history).toString().toUpperCase();
                default:
                    return null;
            }
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {//save fragments
            Object item = super.instantiateItem(container, position);
            fragments.put(position, (HistoryFragment) item);
            return item;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {//destroy fragments
            fragments.remove(position);
            super.destroyItem(container, position, object);
        }

        HistoryFragment getFragment(int position) {
            return fragments.get(position);
        }
    }

}
