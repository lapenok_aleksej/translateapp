package ru.ifmo.lapenok.translate.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import ru.ifmo.lapenok.translate.R;
import ru.ifmo.lapenok.translate.activity.HistoryActivity;

/**
 * Created by Алексей on 07.04.2017.
 */

public class DeleteAllDialogFragment extends DialogFragment {//confirm delete history of translates

    @Override
    @NonNull
    public Dialog onCreateDialog(android.os.Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.delete_all)
                .setTitle(getActivity().getResources().getString(R.string.history))
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ((HistoryActivity) getActivity()).onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, getActivity().getIntent());
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        return builder.create();
    }
}
