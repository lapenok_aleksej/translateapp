package ru.ifmo.lapenok.translate.loaders;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.support.annotation.WorkerThread;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import ru.ifmo.lapenok.translate.R;
import ru.ifmo.lapenok.translate.activity.TranslateActivity;
import ru.ifmo.lapenok.translate.application.TranslateApplication;
import ru.ifmo.lapenok.translate.database.DatabaseHandlerTranslates;
import ru.ifmo.lapenok.translate.database.Lang;
import ru.ifmo.lapenok.translate.database.Translate;
import ru.ifmo.lapenok.translate.utils.DownloadUtils;
import ru.ifmo.lapenok.translate.utils.FileUtils;
import ru.ifmo.lapenok.translate.utils.JsonReader;

/**
 * Created by Алексей on 20.03.2017.
 *
 * @see AsyncTaskLoader
 */

public class TranslateLoader extends AsyncTaskLoader<Translate> {

    private final static String TAG = "TranslateLoader";

    private static final int MAX_SIZE_CACHE = 258;
    private static final int WAIT_THREAD = 1000;

    private final static Map<String, Translate> cache = new ConcurrentHashMap<>();
    private final static Queue<String> forDelete = new ConcurrentLinkedQueue<>();

    private final Context appContext;
    private final String text;
    private String langFrom;
    private final String langTo;
    private final DatabaseHandlerTranslates dbTranslate;
    private boolean detectLang;

    public static final Translate ERROR_TRANSLATE = new Translate(-1, "", "", "", TranslateApplication.getContext().getString(R.string.error_load_translate), 0);

    /**
     * @param context  current context
     * @param text     input string for translate
     * @param langFrom code of language of text
     * @param langTo   code of language of result
     * @see Context
     * @see ru.ifmo.lapenok.translate.database.Lang
     * @see Translate
     */
    public TranslateLoader(@NonNull Context context, String text, String langFrom, String langTo, boolean detectLang) {
        super(context);
        this.text = text.trim();
        this.langFrom = langFrom;
        this.langTo = langTo;
        this.appContext = context;
        this.dbTranslate = TranslateActivity.dbTranslates;
        this.detectLang = detectLang;
        Log.d(TAG, "Translate: contructor complite");
    }

    public String getText() {
        return text;
    }

    /**
     * find in database of translate or load it from internet
     *
     * @return result of translate
     */
    @Override
    @WorkerThread
    public Translate loadInBackground() {
        if (text.equals(""))
            return null;
        try {
            Thread.sleep(WAIT_THREAD);//wait, if user entering text
        } catch (InterruptedException e) {
            Log.e(TAG, "loadInBackground: error to wait new input", e);
            return null;
        }

        if (Thread.interrupted() || isAbandoned() || isReset() || isLoadInBackgroundCanceled())
            return null;

        if (cache.containsKey(text) && !detectLang) {//find on cache
            Translate back = cache.get(text);
            if (back.getFirstCode().equals(langFrom) && back.getSecondCode().equals(langTo))
                return back;
        }

        Translate translate;
        if (detectLang)
            translate = dbTranslate.findTranslate(text, langTo);
        else
            translate = dbTranslate.findTranslate(text, langFrom, langTo);

        if (translate != null) {//find on database
            cache.put(text, translate);//save in cash
            forDelete.add(text);
            if (cache.size() > MAX_SIZE_CACHE)
                cache.remove(forDelete.remove());
            return translate;
        }
        FileInputStream file = null;
        try {
            File tmpFile = FileUtils.createTempExternalFile(appContext, "translate", "jz");
            DownloadUtils.downloadJsonTranslate(langFrom, langTo, text, tmpFile, detectLang);
            file = new FileInputStream(tmpFile);
            Map<String, Object> result = JsonReader.parse(file);
            translate = new Translate(-1, langFrom, langTo, text, (String) result.get("text"), 0);
            if (detectLang) {
                Object result1 = result.get("detected");
                if (result1 instanceof Map) {
                    Map result2 = (Map) result1;
                    if (result2.get("lang") instanceof String && !result2.get("lang").equals(""))
                        translate.setFirstCode((String) (result2).get("lang"));
                }
            }
            if (!TranslateActivity.langs.containsKey(translate.getFirstCode()) || !TranslateActivity.langs.containsKey(translate.getSecondCode())) {//if langs wasn't loaded
                Map<String, Lang> res = new AllLangsLoader(getContext()).loadInBackground();
                if (res != null)
                    TranslateActivity.langs = res;
            }
            if (translate.getOutput().equals(translate.getInput()) && detectLang && !langFrom.equals(translate.getFirstCode())) {//bug of yandex API translate
                //for example, if send request: text='привет', lang=en-el, options=1 then in answer text='привет', what is incorrect because answer is 'γεια'
                langFrom = translate.getFirstCode();
                detectLang = false;
                return loadInBackground();
            }
            cache.put(text, translate);//save in cache
            forDelete.add(text);
            if (cache.size() > MAX_SIZE_CACHE)
                cache.remove(forDelete.remove());

            if (Thread.interrupted() || isAbandoned() || isReset() || isLoadInBackgroundCanceled())
                return null;
            return translate;
        } catch (IOException e) {
            Log.e(TAG, "loadInBackground: " + e.getMessage(), e);
        } finally {
            if (file != null)
                try {
                    file.close();
                } catch (IOException e) {
                    Log.e(TAG, "loadInBackground: " + e.getMessage(), e);
                }
        }
        return ERROR_TRANSLATE;
    }

    @Override
    @UiThread
    protected void onStartLoading() {
        forceLoad();
    }
}
