package ru.ifmo.lapenok.translate.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Алексей on 04.04.2017.
 * Database to contain languages
 *
 * @see Lang
 */

public class DatabaseHandlerLangs extends SQLiteOpenHelper {

    private static final String TAG = "DatabaseLangs";

    //const for configuration database
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "langs";

    //const for names in table
    private static final String TABLE_LANGS = "langs";
    private static final String KEY_ID = "_id";
    private static final String KEY_NAME = "name";
    private static final String KEY_CODE = "code";

    /**
     * create database
     *
     * @param context
     */
    public DatabaseHandlerLangs(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * create tables in sqLiteDatabase
     *
     * @param sqLiteDatabase database
     */
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String createLangsTable = "CREATE TABLE " + TABLE_LANGS + " (" +
                KEY_ID + " INTEGER PRIMARY KEY, " +
                KEY_NAME + " TEXT, " +
                KEY_CODE + " TEXT" + ")";
        sqLiteDatabase.execSQL(createLangsTable);//create table for languages
    }

    /**
     * update tables in sqLiteDatabase
     *
     * @param sqLiteDatabase database
     * @param i              old version of database handler
     * @param i1             new version of database handler
     */
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_LANGS);//update table for languages
        Log.d(TAG, "onUpgrade: tables was deleted");
        onCreate(sqLiteDatabase);

    }

    private Lang getLangByQuery(String query) {
        List<Lang> langs = getAllLangs(query);
        if (langs.size() > 0) {
            return langs.get(0);
        } else
            return null;
    }

    private List<Lang> getAllLangs(String query) {//one function for work with database, when I need to find Languages
        List<Lang> langs = new ArrayList<>();
        SQLiteDatabase db = null;
        Cursor cursor = null;
        try {
            db = this.getReadableDatabase();
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    Lang lang = new Lang(cursor.getString(0), cursor.getString(1));
                    langs.add(lang);
                } while (cursor.moveToNext());
            }
            return langs;
        } catch (SQLiteException e) {
            Log.e(TAG, "getAllLangs: " + e.getMessage(), e);
        } finally {
            if (cursor != null)
                cursor.close();
            if (db != null)
                db.close();
        }
        return langs;
    }

    /**
     * find language in database by code language
     *
     * @param code code of language
     * @return language with code
     * @see Lang
     */
    public Lang getLang(String code) {//find language by code
        String query = "SELECT " + KEY_NAME + " , " + KEY_CODE + " FROM " + TABLE_LANGS + " WHERE " + KEY_CODE + " LIKE '%" + code + "%'";
        return getLangByQuery(query);
    }

    /**
     * load from database all languages
     *
     * @return list of languages
     * @see Lang
     * @see List
     */
    public List<Lang> getAllLangs() {
        String query = "SELECT " + KEY_NAME + ", " + KEY_CODE + " FROM " + TABLE_LANGS;
        return getAllLangs(query);
    }

    /**
     * add into database list of languages
     *
     * @param langs list of languages for add in database
     * @see Lang
     */
    public void add(List<Lang> langs) {//add list of languages
        SQLiteDatabase db = null;
        try {
            db = this.getWritableDatabase();
            db.beginTransaction();
            for (Lang lang : langs) {
                ContentValues values = new ContentValues();
                values.put(KEY_CODE, lang.getCode());
                values.put(KEY_NAME, lang.getName());
                db.insertOrThrow(TABLE_LANGS, null, values);
            }
            db.setTransactionSuccessful();//if all languages was added
        } catch (Exception e) {
            Log.e(TAG, "add: " + e.getMessage(), e);
        } finally {
            if (db != null) {
                db.endTransaction();
                db.close();
            }
        }
    }

    /**
     * add language into database
     *
     * @param data language for add into database
     */
    public void add(Lang data) {
        List<Lang> langs = new ArrayList<>(1);
        langs.add(data);
        add(langs);
    }

    private void deleteAll(String table) {//delete all languages from database
        SQLiteDatabase db = null;
        try {
            db = this.getWritableDatabase();
            db.delete(table, null, null);
        } catch (Exception e) {
            Log.e(TAG, "deleteAllLangs: " + e.getMessage(), e);
        } finally {
            if (db != null)
                db.close();
        }
    }

    /**
     * delete all languages from database
     */
    public void deleteAllLangs() {
        deleteAll(TABLE_LANGS);
    }
}
