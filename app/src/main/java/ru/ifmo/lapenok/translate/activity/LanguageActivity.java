package ru.ifmo.lapenok.translate.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ProgressBar;

import java.util.List;

import ru.ifmo.lapenok.translate.R;
import ru.ifmo.lapenok.translate.database.Lang;
import ru.ifmo.lapenok.translate.loaders.LangsLoader;
import ru.ifmo.lapenok.translate.loaders.callback.CallBack;
import ru.ifmo.lapenok.translate.loaders.callback.ISaveData;

public class LanguageActivity extends AppCompatActivity {

    private static final String TAG = "LanguageActivity";

    public static final String AUTO_LANG_CODE = "auto";

    private String lang, code;

    //const for loader
    private static final int LANGS_LOADER = 665;

    private boolean detectLang;

    //layout views
    RecyclerView recyclerView;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);

        //find layout views
        recyclerView = (RecyclerView) findViewById(R.id.list);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        findViewById(R.id.back).setOnClickListener(new BackOnClickListener());//setOnClickListener for back-button

        Intent intent = getIntent();//load back arguments
        lang = intent.getStringExtra(TranslateActivity.LANG);
        code = intent.getStringExtra(TranslateActivity.CODE);
        detectLang = intent.getBooleanExtra(TranslateActivity.AUTO_DETECT_LANG, false);

        progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(this, R.color.colorPrimaryDark), PorterDuff.Mode.MULTIPLY);

        getSupportLoaderManager().restartLoader(LANGS_LOADER, null, new CallBack<>(new SaveLangs(), new LangsLoader(this), LANGS_LOADER));
        overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
    }

    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(LanguageActivity.this, TranslateActivity.class);
        intent.putExtra(TranslateActivity.AUTO_DETECT_LANG, detectLang);
        startActivity(intent);//go to fist activity
    }

    private class LangOnClickListener implements View.OnClickListener {

        private final Lang language;

        LangOnClickListener(Lang language) {
            this.language = language;
        }

        @Override
        public void onClick(View view) {//send data of chosen language
            finish();
            Intent intent = new Intent(LanguageActivity.this, TranslateActivity.class);
            intent.putExtra(lang, language.getName());
            intent.putExtra(code, language.getCode());
            intent.putExtra(AUTO_LANG_CODE, detectLang);
            startActivity(intent);
            overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);

        }
    }

    private class BackOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            onBackPressed();
        }
    }

    private class SimpleRecycleAdapter extends RecyclerView.Adapter<SimpleRecycleAdapter.ViewHolder> {

        private final List<Lang> langs;//data of languages
        private final LayoutInflater li;

        private SimpleRecycleAdapter(Context context, List<Lang> langs) {
            li = LayoutInflater.from(context);
            this.langs = langs;
            Log.d(TAG, "SimpleRecycleAdapter: size=" + langs.size());
            setHasStableIds(true);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(li.inflate(R.layout.langs_item_list, parent, false));
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            Log.d(TAG, "onBindViewHolder: position=" + position);//show languages
            Lang lang = langs.get(position);
            holder.btn.setText(lang.getName());
            if (lang.getCode().equals(AUTO_LANG_CODE)) {
                holder.sw.setVisibility(View.VISIBLE);
                holder.sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        detectLang = b;
                    }
                });
                holder.btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        detectLang = !detectLang;
                        holder.sw.setChecked(detectLang);
                    }
                });
                holder.sw.setChecked(detectLang);
            } else {
                holder.sw.setVisibility(View.GONE);
                holder.btn.setOnClickListener(new LangOnClickListener(lang));
            }
        }

        @Override
        public long getItemId(int position) {
            return langs.get(position).hashCode();
        }

        @Override
        public int getItemCount() {
            return langs.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            final Button btn;
            final SwitchCompat sw;

            ViewHolder(View itemView) {
                super(itemView);
                btn = (Button) itemView.findViewById(R.id.lang);
                sw = (SwitchCompat) itemView.findViewById(R.id.switch_compat);
            }
        }
    }

    private class SaveLangs implements ISaveData<List<Lang>> {//loaded all languages

        @Override
        public void saveData(List<Lang> data, int id) {
            recyclerView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            recyclerView.setLayoutManager(new LinearLayoutManager(LanguageActivity.this));
            if (code.equals(TranslateActivity.FIRST_LANG_CODE)) {
                data.add(0, new Lang(getString(R.string.auto_lang), AUTO_LANG_CODE));
            }
            SimpleRecycleAdapter adapter = new SimpleRecycleAdapter(LanguageActivity.this, data);
            recyclerView.setAdapter(adapter);
            getSupportLoaderManager().destroyLoader(id);
        }
    }
}
