package ru.ifmo.lapenok.translate.loaders.callback;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;

/**
 * Created by Алексей on 27.03.2017.
 * simplify to work with loaders
 *
 * @see android.support.v4.app.LoaderManager.LoaderCallbacks
 * @see android.support.v4.content.AsyncTaskLoader
 */

public class CallBack<T> implements LoaderManager.LoaderCallbacks<T> {

    private static final String TAG = "CallBack";

    private final ISaveData<T> saveData;
    private final Loader<T> loader;
    private final int id;

    /**
     * create new call back
     *
     * @param saveData what will be called after finish loader
     * @param loader   loader for run
     * @param id       id of loader
     * @see Loader
     * @see ISaveData
     */
    public CallBack(ISaveData<T> saveData, Loader<T> loader, int id) {
        this.saveData = saveData;
        this.loader = loader;
        this.id = id;
    }

    /**
     * return loader if id of loader is id
     *
     * @param id   id of loader
     * @param args ignore this param
     * @return loader
     * @see Loader
     */
    @Override
    public Loader<T> onCreateLoader(int id, Bundle args) {
        if (id == this.id)
            return loader;
        else {
            Log.e(TAG, "onCreateLoader: unknown id", new CallBackException("Unknown id"));
            return null;
        }
    }

    /**
     * call save data of result
     *
     * @param loader current loader
     * @param data   result of loader
     * @see Loader
     * @see ISaveData
     */
    @Override
    public void onLoadFinished(Loader<T> loader, T data) {
        saveData.saveData(data, id);
    }

    @Override
    public void onLoaderReset(Loader<T> loader) {
        loader.cancelLoad();
    }
}
