package ru.ifmo.lapenok.translate.loaders.callback;

/**
 * Created by Алексей on 27.03.2017.
 * throws if something happen wrong in CallBack
 *
 * @see CallBack
 */

class CallBackException extends Exception {

    CallBackException() {
        super("");
    }

    CallBackException(String msg) {
        super(msg);
    }
}
