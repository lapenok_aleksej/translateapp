package ru.ifmo.lapenok.translate;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import ru.ifmo.lapenok.translate.activity.TranslateActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.action.ViewActions.swipeRight;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by Алексей on 02.04.2017.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class TranslateActivityTest {
    @Rule
    public ActivityTestRule<TranslateActivity> mActivityRule = new ActivityTestRule<>(TranslateActivity.class);

    @Test
    public void clearButtonTest() {

        String firstLang = ((Button) findViewById(R.id.first_lang)).getText().toString();
        String secondLang = ((Button) findViewById(R.id.second_lang)).getText().toString();

        onView(withId(R.id.clear)).perform(click());

        onView(withId(R.id.input)).check(matches(withText("")));
        onView(withId(R.id.output)).check(matches(withText("")));
        onView(withId(R.id.first_lang)).check(matches(withText(firstLang)));
        onView(withId(R.id.second_lang)).check(matches(withText(secondLang)));
    }

    @Test
    public void clearSwipeTest() {
        String firstLang = ((Button) findViewById(R.id.first_lang)).getText().toString();
        String secondLang = ((Button) findViewById(R.id.second_lang)).getText().toString();
        onView(withId(R.id.input)).perform(swipeLeft());
        onView(withId(R.id.input)).check(matches(withText("")));
        onView(withId(R.id.output)).check(matches(withText("")));
        onView(withId(R.id.first_lang)).check(matches(withText(firstLang)));
        onView(withId(R.id.second_lang)).check(matches(withText(secondLang)));
    }

    @Test
    public void previousSwipeTest() {
        translateSimpleTest();

        String firstLang = ((Button) findViewById(R.id.first_lang)).getText().toString();
        String secondLang = ((Button) findViewById(R.id.second_lang)).getText().toString();
        clearSwipeTest();

        onView(withId(R.id.input)).perform(swipeRight());
        if (firstLang.equals("Русский") && secondLang.equals("Английский")) {
            onView(withId(R.id.input)).check(matches(withText("привет")));
            onView(withId(R.id.output)).check(matches(withText("hi")));
        }
        if (firstLang.equals("Английский") && secondLang.equals("Русский")) {
            onView(withId(R.id.output)).check(matches(withText("привет")));
            onView(withId(R.id.input)).check(matches(withText("hi")));
        }

        onView(withId(R.id.first_lang)).check(matches(withText(firstLang)));
        onView(withId(R.id.second_lang)).check(matches(withText(secondLang)));
    }

    @Test
    public void translateSimpleTest() {
        String firstLang = ((Button) findViewById(R.id.first_lang)).getText().toString();
        String secondLang = ((Button) findViewById(R.id.second_lang)).getText().toString();

        if (firstLang.toUpperCase().equals("РУССКИЙ"))
            onView(withId(R.id.input)).perform(replaceText("привет"));
        else if (firstLang.toUpperCase().equals("АНГЛИЙСКИЙ"))
            onView(withId(R.id.input)).perform(replaceText("hi"));

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (secondLang.toUpperCase().equals("РУССКИЙ"))
            onView(withId(R.id.output)).check(matches(withText("привет")));
        else if (secondLang.toUpperCase().equals("АНГЛИЙСКИЙ"))
            onView(withId(R.id.output)).check(matches(withText("hi")));
        onView(withId(R.id.first_lang)).check(matches(withText(firstLang)));
        onView(withId(R.id.second_lang)).check(matches(withText(secondLang)));
    }

    @Test
    public void swapTest() {
        String firstLang = ((Button) findViewById(R.id.first_lang)).getText().toString();
        String secondLang = ((Button) findViewById(R.id.second_lang)).getText().toString();
        String input = ((EditText) findViewById(R.id.input)).getText().toString();
        String output = ((TextView) findViewById(R.id.output)).getText().toString();

        onView(withId(R.id.swap)).perform(click());

        onView(withId(R.id.first_lang)).check(matches(withText(secondLang)));
        onView(withId(R.id.second_lang)).check(matches(withText(firstLang)));
        onView(withId(R.id.input)).check(matches(withText(output)));
        onView(withId(R.id.output)).check(matches(withText(input)));
    }

    @Test
    public void changeLanguage() {
        clearButtonTest();
        onView(withId(R.id.second_lang)).perform(click());
        onView(withId(R.id.list)).check(matches(isDisplayed()));
        onView(withText("Английский")).perform(click());
        onView(withId(R.id.second_lang)).check(matches(withText("Английский")));
        onView(withId(R.id.first_lang)).perform(click());
        onView(withText("Арабский")).perform(click());
        onView(withId(R.id.first_lang)).check(matches(withText("Арабский")));

        onView(withId(R.id.input)).perform(replaceText("привет"));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.first_lang)).check(matches(withText("Русский")));//auto-detect lang
    }

    private View findViewById(int id) {
        return mActivityRule.getActivity().findViewById(id);
    }
}
