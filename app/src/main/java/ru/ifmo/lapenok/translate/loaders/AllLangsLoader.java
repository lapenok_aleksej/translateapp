package ru.ifmo.lapenok.translate.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.ifmo.lapenok.translate.activity.TranslateActivity;
import ru.ifmo.lapenok.translate.database.DatabaseHandlerLangs;
import ru.ifmo.lapenok.translate.database.Lang;

/**
 * Created by Алексей on 27.03.2017.
 *
 * @see AsyncTaskLoader
 */
public class AllLangsLoader extends AsyncTaskLoader<Map<String, Lang>> {

    private final DatabaseHandlerLangs db;

    public AllLangsLoader(Context context) {
        super(context);
        db = TranslateActivity.dbLangs;
    }

    /**
     * load from database languages or from internet
     *
     * @return map (code language) -> Lang
     * @see Lang
     */
    @Override
    public Map<String, Lang> loadInBackground() {
        List<Lang> langs = db.getAllLangs();
        if (langs == null || langs.size() < 50) {
            LangsLoader getLangLoader = new LangsLoader(getContext());
            langs = getLangLoader.loadInBackground();
        }
        if (langs == null)
            return null;
        final Map<String, Lang> result = new HashMap<>();
        for (Lang lang : langs) {
            result.put(lang.getCode(), lang);
        }
        return result;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }
}
