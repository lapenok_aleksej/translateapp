package ru.ifmo.lapenok.translate.loaders;

import android.content.Context;
import android.support.annotation.UiThread;
import android.support.annotation.WorkerThread;
import android.support.v4.content.AsyncTaskLoader;

import ru.ifmo.lapenok.translate.activity.TranslateActivity;
import ru.ifmo.lapenok.translate.database.DatabaseHandlerTranslates;
import ru.ifmo.lapenok.translate.database.Translate;

/**
 * Created by Алексей on 27.03.2017.
 *
 * @see AsyncTaskLoader
 */

public class SaveTranslateLoader extends AsyncTaskLoader<Void> {

    private final DatabaseHandlerTranslates db;
    private final Translate translate;

    /**
     * @param context   current context
     * @param translate argument to save
     * @see Translate
     * @see AsyncTaskLoader
     */
    public SaveTranslateLoader(Context context, Translate translate) {
        super(context);
        db = TranslateActivity.dbTranslates;
        this.translate = translate;
    }

    /**
     * save translate in database if it doesn't exist in it
     */
    @Override
    @WorkerThread
    public Void loadInBackground() {
        if (db.findTranslate(translate.getInput(), translate.getFirstCode(), translate.getSecondCode()) == null) // if this translate exist in db
            db.add(translate);//save in database
        return null;
    }

    @Override
    @UiThread
    public void onStartLoading() {
        forceLoad();
    }
}
