package ru.ifmo.lapenok.translate.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import java.util.ArrayList;
import java.util.List;

import ru.ifmo.lapenok.translate.activity.TranslateActivity;
import ru.ifmo.lapenok.translate.database.DatabaseHandlerTranslates;
import ru.ifmo.lapenok.translate.database.Translate;

/**
 * Created by Алексей on 07.04.2017.
 *
 * @see AsyncTaskLoader
 */
public class FavouriteTranslatesLoader extends AsyncTaskLoader<List<Translate>> {

    private final DatabaseHandlerTranslates db;

    public FavouriteTranslatesLoader(Context context) {
        super(context);
        db = TranslateActivity.dbTranslates;
    }

    /**
     * load favourite translates from database
     *
     * @return List of Translates
     * @see Translate
     * @see List
     */
    @Override
    public List<Translate> loadInBackground() {
        List<Translate> allTranslates = TranslateActivity.translates;
        List<Translate> favorite = new ArrayList<>();
        for (Translate translate : allTranslates) {
            if (translate.getFavourite() == 1)
                favorite.add(translate);
        }
        return favorite;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }
}
