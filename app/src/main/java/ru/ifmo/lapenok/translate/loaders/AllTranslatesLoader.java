package ru.ifmo.lapenok.translate.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import java.util.List;

import ru.ifmo.lapenok.translate.activity.TranslateActivity;
import ru.ifmo.lapenok.translate.database.DatabaseHandlerTranslates;
import ru.ifmo.lapenok.translate.database.Translate;

/**
 * Created by Алексей on 27.03.2017.
 *
 * @see AsyncTaskLoader
 */

public class AllTranslatesLoader extends AsyncTaskLoader<List<Translate>> {

    private final DatabaseHandlerTranslates db;

    public AllTranslatesLoader(Context context) {
        super(context);
        db = TranslateActivity.dbTranslates;
    }

    /**
     * load history of translates from database
     *
     * @return List of translates
     * @see Translate
     */
    @Override
    public List<Translate> loadInBackground() {
        return db.getAllTranslations();
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }
}
