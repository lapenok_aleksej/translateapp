package ru.ifmo.lapenok.translate.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.ifmo.lapenok.translate.R;
import ru.ifmo.lapenok.translate.database.DatabaseHandlerLangs;
import ru.ifmo.lapenok.translate.database.DatabaseHandlerTranslates;
import ru.ifmo.lapenok.translate.database.Lang;
import ru.ifmo.lapenok.translate.database.Translate;
import ru.ifmo.lapenok.translate.loaders.AllLangsLoader;
import ru.ifmo.lapenok.translate.loaders.AllTranslatesLoader;
import ru.ifmo.lapenok.translate.loaders.SaveTranslateLoader;
import ru.ifmo.lapenok.translate.loaders.TranslateLoader;
import ru.ifmo.lapenok.translate.loaders.UpdateTranslateLoader;
import ru.ifmo.lapenok.translate.loaders.callback.CallBack;
import ru.ifmo.lapenok.translate.loaders.callback.ISaveData;

public class TranslateActivity extends AppCompatActivity {

    private static final String TAG = "TranslateActivity";

    //const for save and send data
    public static final String FIRST_LANG = "first_lang";
    public static final String SECOND_LANG = "second_lang";
    public static final String FIRST_LANG_CODE = "first_lang_code";
    public static final String SECOND_LANG_CODE = "second_lang_code";
    public static final String INPUT = "input";
    public static final String OUTPUT = "output";
    public static final String POSITION = "position";
    public static final String FAVOURITE = "favourite";
    public static final String LANG = "lang";
    public static final String CODE = "code";
    public static final String AUTO_DETECT_LANG = "auto_detect_lang";

    //const for loaders
    private static final int TRANSLATE_LOADER = 0;
    private static final int SAVE_TRANSLATE_LOADER = 2;
    private static final int All_TRANSLATES_LOADER = 4;
    private static final int AlL_LANGS_LOADER = 5;
    private static final int UPDATE_TRANSLATE_LOADER = 6;
    private static final int DETECT_LANG_LOADER = 7;

    //layout views
    Button firstLang;
    Button secondLang;
    EditText input;
    TextView output;
    RatingBar favouriteBar;
    RelativeLayout linelInput;

    //local data
    private Lang first, second;
    private String inputTxt, outputTxt;
    private int favourite;
    private boolean detectLang;
    private boolean needToSave;

    //history of translates
    public static List<Translate> translates = new ArrayList<>();
    //number of current translate (can be -1, when history not loaded or translates.size, when current translate didn't saved)
    int position;
    //map to find Language for code
    public static Map<String, Lang> langs = new HashMap<>();

    //database handles
    public static DatabaseHandlerTranslates dbTranslates = null;
    public static DatabaseHandlerLangs dbLangs = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_translate);

        needToSave = true;
        //create databaseHandlers
        if (dbTranslates == null)
            dbTranslates = new DatabaseHandlerTranslates(this);
        if (dbLangs == null)
            dbLangs = new DatabaseHandlerLangs(this);

        if (translates.size() == 0) {//load from database history of translates
            AllTranslatesLoader allTranslatesLoader = new AllTranslatesLoader(this);
            getSupportLoaderManager().restartLoader(All_TRANSLATES_LOADER, null, new CallBack<>(new SaveAllTranslates(), allTranslatesLoader, All_TRANSLATES_LOADER));
        }

        if (langs.size() == 0) {//load from database or from internet list of languages
            AllLangsLoader allLangsLoader = new AllLangsLoader(this);
            getSupportLoaderManager().restartLoader(AlL_LANGS_LOADER, null, new CallBack<>(new SaveLangs(), allLangsLoader, AlL_LANGS_LOADER));
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);//include toolbar
        setSupportActionBar(toolbar);

        firstLang = (Button) findViewById(R.id.first_lang);//find views of layout
        secondLang = (Button) findViewById(R.id.second_lang);
        input = (EditText) findViewById(R.id.input);
        output = (TextView) findViewById(R.id.output);
        favouriteBar = (RatingBar) findViewById(R.id.favourite);
        linelInput = (RelativeLayout) findViewById(R.id.linelInput);

        firstLang.setOnClickListener(new LangOnClickListener());//set onClickListener for change languages
        secondLang.setOnClickListener(new LangOnClickListener());

        //get settings
        final SharedPreferences settings = getSharedPreferences(TAG, 0);
        first = new Lang(settings.getString(FIRST_LANG, getString(R.string.auto_lang)),
                settings.getString(FIRST_LANG_CODE, LanguageActivity.AUTO_LANG_CODE));
        second = new Lang(settings.getString(SECOND_LANG, "English"),
                settings.getString(SECOND_LANG_CODE, "en"));
        detectLang = settings.getBoolean(AUTO_DETECT_LANG, true);
        inputTxt = settings.getString(INPUT, "");
        outputTxt = settings.getString(OUTPUT, "");
        favourite = settings.getInt(FAVOURITE, 0);
        position = settings.getInt(POSITION, -1);
        if (position != -1 && position < translates.size())//if was pressed back button
            setValues(translates.get(position));

        final Intent intent = getIntent();//load from dialog data
        boolean oldDetect = detectLang;
        detectLang = intent.getBooleanExtra(AUTO_DETECT_LANG, detectLang);
        boolean isNeedToTranslate = oldDetect ^ detectLang;
        if (intent.getStringExtra(FIRST_LANG) != null) {//if first language changed
            first = new Lang(intent.getStringExtra(FIRST_LANG),
                    intent.getStringExtra(FIRST_LANG_CODE));
            isNeedToTranslate = true;
        }
        if (intent.getStringExtra(SECOND_LANG) != null) {//if second language changed
            second = new Lang(intent.getStringExtra(SECOND_LANG),
                    intent.getStringExtra(SECOND_LANG_CODE));
            isNeedToTranslate = true;
        }
        if (intent.getIntExtra(POSITION, -1) != -1) {//if shown translate changed
            position = intent.getIntExtra(POSITION, -1);
            Translate translate = translates.get(position);
            setValues(translate);
            isNeedToTranslate = false;
        }
        if (isNeedToTranslate) {
            outputTxt = "";
            translate();
        }


        showData();//show for user data

        findViewById(R.id.swap).setOnClickListener(new SwapOnClickListener());//setOnClickListener for swap languages button

        findViewById(R.id.clear).setOnClickListener(new ClearOnClickListener());//setOnClickListener for clear input button

        favouriteBar.setOnTouchListener(new FavoriteOnTouchListener());//setOnClickListener for favourite button

        input.addTextChangedListener(new InputTextWatcher());//set TextWatcher for auto-translate
        input.setOnTouchListener(new SwipeListener(this));//set OnTouchListener for input folder

        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);//show animation
        Log.d(TAG, "onCreate: finish");
    }

    private void showData() {//show user data
        input.setText(inputTxt);
        output.setText(outputTxt);
        firstLang.setText(first.getName());
        secondLang.setText(second.getName());
        favouriteBar.setRating(favourite);
        input.setSelection(inputTxt.length());
    }

    private void setValues(Translate translate) {//set values from Translate
        outputTxt = translate.getOutput();
        if (translate == TranslateLoader.ERROR_TRANSLATE) {
            return;
        }
        if (langs != null && langs.containsKey(translate.getFirstCode()) && langs.containsKey(translate.getSecondCode())) {
            first = langs.get(translate.getFirstCode());
            second = langs.get(translate.getSecondCode());
        } else {
            Toast.makeText(this, R.string.error_load, Toast.LENGTH_LONG).show();
            return;
        }
        inputTxt = translate.getInput();
        favourite = translate.getFavourite();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.exit:
                finish();
                return true;
            case R.id.history:
            case R.id.favourite:
                Intent intent = new Intent(TranslateActivity.this, HistoryActivity.class);
                intent.addFlags(item.getItemId() == R.id.history ? HistoryActivity.FRAGMENT_HISTORY : HistoryActivity.FRAGMENT_FAVORITE);
                startActivity(intent);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {//save all data
        super.onPause();
        SharedPreferences settings = getSharedPreferences(TAG, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(FIRST_LANG, first.getName());
        editor.putString(FIRST_LANG_CODE, first.getCode());
        editor.putString(SECOND_LANG, second.getName());
        editor.putString(SECOND_LANG_CODE, second.getCode());
        editor.putString(INPUT, inputTxt);
        editor.putString(OUTPUT, outputTxt);
        editor.putInt(FAVOURITE, favourite);
        editor.putInt(POSITION, position);
        editor.putBoolean(AUTO_DETECT_LANG, detectLang);
        editor.apply();
        saveTranslate();
    }

    private void translate() { //translate input text
        if (!inputTxt.equals("")) {
            position = translates.size();
            TranslateLoader translateLoader = new TranslateLoader(this, inputTxt, first.getCode().equals(LanguageActivity.AUTO_LANG_CODE) ? null : first.getCode(), second.getCode(), detectLang);//create loader for load translate
            getSupportLoaderManager().destroyLoader(TRANSLATE_LOADER);//stop previous loader
            getSupportLoaderManager().restartLoader(TRANSLATE_LOADER, null, new CallBack<>(new SaveDataTranslate(), translateLoader, TRANSLATE_LOADER));//start thread to translate
        } else {
            outputTxt = "";
            showData();
        }
    }

    private void clear() {//clear all data
        saveTranslate();//save current translate and then clear data
        inputTxt = "";
        outputTxt = "";
        favourite = 0;
        position = translates.size();
    }

    private void saveTranslate() {//save current translate
        if (inputTxt.equals("") || outputTxt.equals("") || position < translates.size() || !needToSave)//if translate is empty, then I don't want to save it
            return;
        translates.add(new Translate(-1, first.getCode(), second.getCode(), inputTxt, outputTxt, favourite));//save in cash
        position = translates.size() - 1;//set position
        SaveTranslateLoader saveTranslateLoader = new SaveTranslateLoader(this, translates.get(position));//create loader for save current translate into database
        getSupportLoaderManager().restartLoader(SAVE_TRANSLATE_LOADER, null, new CallBack<>(new SaveDataVoid(), saveTranslateLoader, SAVE_TRANSLATE_LOADER));//run loader
    }

    private class SaveDataTranslate implements ISaveData<Translate> {//on loaded translate

        @Override
        public void saveData(Translate data, int id) {
            Loader loader = getSupportLoaderManager().getLoader(id);
            if (loader instanceof TranslateLoader) {
                TranslateLoader translateLoader = (TranslateLoader) loader;
                if (translateLoader.getText().equals(input.getText().toString().trim()) && data != null) {//if user didn't change input data, then show translate
                    setValues(data);
                    needToSave = data != TranslateLoader.ERROR_TRANSLATE;//don't save error translate
                    showData();
                }
            }
            getSupportLoaderManager().destroyLoader(id);
        }
    }

    private class SaveDataVoid implements ISaveData<Void> {//finish loader with return-type void

        @Override
        public void saveData(Void data, int id) {
            getSupportLoaderManager().destroyLoader(id);
        }
    }

    private class SaveAllTranslates implements ISaveData<List<Translate>> {//on loaded all translations from db

        @Override
        public void saveData(List<Translate> data, int id) {
            translates = data;
            if (position == -1)
                position = data.size();
            getSupportLoaderManager().destroyLoader(id);
        }
    }

    private class SaveLangs implements ISaveData<Map<String, Lang>> {//on loaded all languages

        @Override
        public void saveData(Map<String, Lang> data, int id) {
            if (data == null) {
                Toast toast = Toast.makeText(TranslateActivity.this, R.string.error_load_langs, Toast.LENGTH_LONG);
                toast.show();
            } else {
                langs = data;
            }
            getSupportLoaderManager().destroyLoader(id);
        }
    }

    private class AnimationListener implements Animation.AnimationListener {
        @Override
        public void onAnimationStart(Animation animation) {
            Log.d(TAG, "onAnimationStart: ");
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            Log.d(TAG, "onAnimationEnd: ");
            showData();//show new data after animation
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
            Log.d(TAG, "onAnimationRepeat: ");
        }
    }

    private class SwipeListener implements View.OnTouchListener {
        private final GestureDetector gestureDetector;

        SwipeListener(Context ctx) {
            gestureDetector = new GestureDetector(ctx, new GestureListener());
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            /*LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) linelInput.getLayoutParams();
            switch (event.getAction()) {
                case MotionEvent.ACTION_MOVE:
                    params.leftMargin = (int) event.getRawX() - (v.getWidth() / 2);//move input line with finger of user
                    break;
                case MotionEvent.ACTION_UP:
                    params.leftMargin = 0;//move input line to old place
                    break;
                case MotionEvent.ACTION_DOWN:
                    break;
            }
            linelInput.setLayoutParams(params);*/
            v.onTouchEvent(event);//call base onTouch method
            return gestureDetector.onTouchEvent(event);//use for scroll
        }

        private final class GestureListener extends GestureDetector.SimpleOnGestureListener {

            private static final int SWIPE_THRESHOLD = 100;
            private static final int SWIPE_VELOCITY_THRESHOLD = 100;

            @Override
            public boolean onDown(MotionEvent e) {
                return true;
            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                boolean result = false;
                try {
                    float diffY = e2.getY() - e1.getY();
                    float diffX = e2.getX() - e1.getX();
                    if (Math.abs(diffX) > Math.abs(diffY)) {//vertical or horizontal swipe
                        if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {//if it swipe
                            if (diffX > 0) {
                                onSwipeRight();
                            } else {
                                onSwipeLeft();
                            }
                            result = true;
                        }
                    } else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffY > 0) {
                            onSwipeBottom();
                        } else {
                            onSwipeTop();
                        }
                        return true;
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
                return result;
            }
        }


        void onSwipeRight() {//previous translate
            if (langs.size() > 0 && translates.size() > 0 && position > 0) {
                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_right);
                linelInput.clearAnimation();
                Translate translate = translates.get(--position);
                animation.setAnimationListener(new AnimationListener());
                setValues(translate);
                linelInput.startAnimation(animation);
            }
            Log.d(TAG, "onSwipeRight: ");
        }

        void onSwipeLeft() {//clean translate
            if (inputTxt.equals(""))
                return;
            Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_left);
            linelInput.clearAnimation();
            clear();
            animation.setAnimationListener(new AnimationListener());
            linelInput.startAnimation(animation);
            Log.d(TAG, "onSwipeLeft: ");
        }

        void onSwipeTop() {//not use
            Log.d(TAG, "onSwipeTop: ");
        }

        void onSwipeBottom() {//not use
            Log.d(TAG, "onSwipeBottom: ");

        }
    }

    private class FavoriteOnTouchListener implements View.OnTouchListener {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {//onclick favourite button
                favourite = 1 - favourite; //change favourite
                favouriteBar.setRating(favourite);
                if (position != translates.size()) {
                    Translate translate = translates.get(position);
                    translate.setFavourite(favourite);
                    UpdateTranslateLoader loader = new UpdateTranslateLoader(TranslateActivity.this, translate);
                    getSupportLoaderManager().restartLoader(UPDATE_TRANSLATE_LOADER, null, new CallBack<>(new SaveDataVoid(), loader, UPDATE_TRANSLATE_LOADER));//save in db this update
                }
            }
            return true;
        }
    }

    private class SwapOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            if (first.getCode().equals(LanguageActivity.AUTO_LANG_CODE))
                return;
            Lang langTmp = first;//swap languages
            first = second;
            second = langTmp;

            String tmp;
            tmp = inputTxt;//swap input and output
            inputTxt = outputTxt;
            outputTxt = tmp;

            position = translates.size();//new translation

            showData();
        }
    }

    private class ClearOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {//on click clear button
            clear();
            showData();
        }
    }

    private class LangOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {//user want to change language
            needToSave = false;
            finish();
            Intent intent = new Intent(TranslateActivity.this, LanguageActivity.class);
            if (view.getId() == firstLang.getId()) {//change first language
                intent.putExtra(LANG, FIRST_LANG);
                intent.putExtra(CODE, FIRST_LANG_CODE);
            } else {//change second language
                intent.putExtra(LANG, SECOND_LANG);
                intent.putExtra(CODE, SECOND_LANG_CODE);
            }
            intent.putExtra(AUTO_DETECT_LANG, detectLang);
            startActivity(intent);
        }
    }

    private class InputTextWatcher implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (input.getText().toString().equals(inputTxt))//if input was changed by method setText
                return;
            inputTxt = input.getText().toString();
            if (inputTxt.contains("\n")) {//ignore key enter
                inputTxt = inputTxt.replaceAll("\n", "");
                showData();
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);//hide keyboard
                try {
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                } catch (NullPointerException e) {
                    Log.e(TAG, "afterTextChanged: " + e.getMessage(), e);
                }
            }
            translate();
        }
    }
}
