package ru.ifmo.lapenok.translate.utils;

import android.content.Context;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by Алексей on 18.03.2017.
 * Util to create tmp file
 */

public final class FileUtils {

    /**
     * @param context   current context
     * @param prefix    name of file
     * @param extension extension of file
     * @return tmp file
     * @throws IOException if can't create this tmp file
     */
    public static File createTempExternalFile(Context context, String prefix, String extension) throws IOException {
        File dir = context.getExternalFilesDir(null);
        if (dir == null) {
            throw new FileNotFoundException("External file dir is null");
        }
        if (dir.exists() && !dir.isDirectory()) {
            throw new IOException("Not a directory: " + dir);
        }
        if (!dir.exists() && !dir.mkdirs()) {
            throw new IOException("Failed to create directory: " + dir);
        }
        return File.createTempFile(prefix, extension, dir);
    }
}
