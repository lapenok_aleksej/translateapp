package ru.ifmo.lapenok.translate.utils;

import android.util.Log;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Алексей on 18.03.2017.
 * simple class to load from internet some data
 */

public final class DownloadUtils {

    private static final String TAG = "DownloadUtils";

    //const of yandex translate API
    private static final String KEY = "trnsl.1.1.20170315T213223Z.8913d59070d9554a.4726cf201a5e11fdb8fb9c843421b6f91e943f40";
    private static final String URL_GET_LANGS = "https://translate.yandex.net/api/v1.5/tr.json/getLangs?key=" + KEY;
    private static final String URL_GET_LANG = "https://translate.yandex.net/api/v1.5/tr.json/detect?key=" + KEY;
    private static final String URL_GET_TRASLATE = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=" + KEY; //address to translate text

    private static final String KEY_DICTONARY = "dict.1.1.20170317T111150Z.0281f6334a1e76ca.b7f3a9aee9a0ed79ec56036386169305fc8866af";
    private static final String URL_TRASLATE_WORD = "https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=" + KEY_DICTONARY;

    /**
     * load into tmp result of translate
     *
     * @param langFrom code of input language
     * @param langTo   code of result language
     * @param input    string for translate
     * @param tmp      file to write result
     * @throws IOException if code of answer of server wasn't 200 or was IOException when write data in file
     * @see IOException
     */
    public static void downloadJsonTranslate(String langFrom, String langTo, String input, File tmp, boolean detectLang) throws IOException {
        String downloadUrl = URL_GET_TRASLATE + "&lang=" + (langFrom != null && !langFrom.equals("") ? langFrom + "-" : "") + langTo + (detectLang ? "&options=1" : "");
        input = "text=" + URLEncoder.encode(input, "UTF-8");
        downloadJson(downloadUrl, input, tmp);
    }

    /**
     * load language of input string in tmp file
     *
     * @param input string for detect language
     * @param tmp   file to write result
     * @throws IOException if code of answer of server wasn't 200 or was IOException when write data in file
     * @see IOException
     */
    public static void downloadJsonLang(String input, File tmp) throws IOException {
        input = "text=" + URLEncoder.encode(input, "UTF-8");
        downloadJson(URL_GET_LANG, input, tmp);
    }

    /**
     * load list of languages in tmp file
     *
     * @param lang code of UI language
     * @param tmp  file to write result
     * @throws IOException if code of answer of server wasn't 200 or was IOException when write data in file
     * @see IOException
     */
    public static void downloadJsonLangs(String lang, File tmp) throws IOException {
        String downloadUrl = URL_GET_LANGS + "&ui=" + lang;
        downloadJson(downloadUrl, "", tmp);
    }


    private static void downloadJson(String downloadUrl, String input, File tmp) throws IOException {
        Log.d(TAG, "downloadJsonTranslate: start downloading, url=" + downloadUrl);
        HttpsURLConnection connection = (HttpsURLConnection) new URL(downloadUrl).openConnection();

        InputStream in = null;
        OutputStream out = null;

        DataOutputStream dataOutputStream = null;

        try {
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            dataOutputStream = new DataOutputStream(connection.getOutputStream());
            dataOutputStream.writeBytes(input);
            Log.d(TAG, "downloadJsonTranslate: data was sended");

            int responseCode = connection.getResponseCode();
            Log.d(TAG, "downloadJsonTranslate: Received HTTPs response code=" + responseCode);
            if (responseCode != HttpsURLConnection.HTTP_OK) {
                throw new FileNotFoundException("Unexpected HTTP response: " + responseCode
                        + ", " + connection.getResponseMessage());
            }

            int contentLength = connection.getContentLength();
            Log.d(TAG, "downloadJson: length=" + contentLength);

            in = connection.getInputStream();

            out = new FileOutputStream(tmp);

            // Размер полученной порции в байтах
            int receivedBytes;
            // Сколько байт всего получили (и записали).
            int receivedLength = 0;
            // прогресс скачивания от 0 до 100
            int progress = 0;
            byte[] buffer = new byte[1024 * 8];

            while ((receivedBytes = in.read(buffer)) >= 0) {
                out.write(buffer, 0, receivedBytes);
                receivedLength += receivedBytes;

                if (contentLength > 0) {
                    int newProgress = 100 * receivedLength / contentLength;
                    if (newProgress > progress) {
                        Log.d(TAG, "Downloaded " + newProgress + "% of " + contentLength + " bytes");
                    }
                    progress = newProgress;
                }
            }
            if (receivedLength != contentLength) {
                Log.w(TAG, "Received " + receivedLength + " bytes, but expected " + contentLength);
            } else {
                Log.d(TAG, "Received " + receivedLength + " bytes");
            }
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    Log.e(TAG, "downloadJson: Failed to close HTTP input stream", e);
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    Log.e(TAG, "downloadJson: Failed to close output file", e);
                }
            }
            if (dataOutputStream != null) {
                try {
                    dataOutputStream.close();
                } catch (IOException e) {
                    Log.e(TAG, "downloadJson: Failed to close HTTP output stream", e);
                }
            }
        }
    }
}
