package ru.ifmo.lapenok.translate.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.ifmo.lapenok.translate.database.Lang;

/**
 * Created by Алексей on 18.03.2017.
 * Util to parse json
 */

public final class JsonReader {

    private final static String TAG = "JsonRead";

    /**
     * parse json from inputStream
     *
     * @param inputStream input file
     * @return map of json struct
     * @throws IOException if can't read file
     */
    public static Map<String, Object> parse(InputStream inputStream) throws IOException {
        android.util.JsonReader reader = new android.util.JsonReader(new InputStreamReader(inputStream));
        reader.beginObject();
        Map<String, Object> result = new HashMap<>();
        while (reader.hasNext()) {
            final String name = reader.nextName();
            if (name == null) {
                reader.skipValue();
                continue;
            }

            switch (name) {
                case "code":
                    result.put(name, reader.nextInt());
                    break;
                case "lang":
                    result.put(name, reader.nextString());
                    break;
                case "text":
                    reader.beginArray();
                    result.put(name, reader.nextString());
                    reader.endArray();
                    break;
                case "dirs":
                    result.put(name, parseDirs(reader));
                    break;
                case "langs":
                    result.put(name, parseLangs(reader));
                    break;
                case "message":
                    result.put(name, reader.nextString());
                    break;
                case "detected":
                    result.put(name, parseDetected(reader));
                    break;
                default:
                    reader.skipValue();
                    break;
            }
        }
        reader.endObject();
        return result;
    }

    private static Map<String, String> parseDetected(android.util.JsonReader reader) throws IOException {
        reader.beginObject();
        Map<String, String> result = new HashMap<>();
        while (reader.hasNext()) {
            final String name = reader.nextName();
            if (name == null) {
                reader.skipValue();
                continue;
            }
            switch (name) {
                case "lang":
                    result.put(name, reader.nextString());
                    break;
                default:
                    reader.skipValue();
                    break;
            }
        }
        reader.endObject();
        return result;
    }

    private static String[] parseDirs(android.util.JsonReader reader) throws IOException {
        reader.beginArray();
        List<String> dirs = new ArrayList<>();
        while (reader.hasNext()) {
            dirs.add(reader.nextString());
        }
        reader.endArray();
        return dirs.toArray(new String[dirs.size()]);
    }

    private static List<Lang> parseLangs(android.util.JsonReader reader) throws IOException {
        reader.beginObject();
        List<Lang> langs = new ArrayList<>(100);
        while (reader.hasNext()) {
            String code = reader.nextName();
            String name = reader.nextString();
            langs.add(new Lang(name, code));
        }
        reader.endObject();
        return langs;
    }
}
