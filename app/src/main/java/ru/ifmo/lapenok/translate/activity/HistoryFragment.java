package ru.ifmo.lapenok.translate.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import ru.ifmo.lapenok.translate.R;
import ru.ifmo.lapenok.translate.database.Lang;
import ru.ifmo.lapenok.translate.database.Translate;
import ru.ifmo.lapenok.translate.loaders.AllLangsLoader;
import ru.ifmo.lapenok.translate.loaders.FavouriteTranslatesLoader;
import ru.ifmo.lapenok.translate.loaders.UpdateTranslateLoader;
import ru.ifmo.lapenok.translate.loaders.callback.CallBack;
import ru.ifmo.lapenok.translate.loaders.callback.ISaveData;

public class HistoryFragment extends Fragment {

    private static final String TAG = "HistoryFragment";

    public static final String TYPE = "type";

    private static final int GET_FAVOURITE_TRANSLATES_LOADER = 0;
    private static final int GET_ALL_LANGS = 1;
    private static final int UPDATE_TRANSLATE = 2;

    RecyclerView recyclerView;
    ProgressBar progressBar;
    TextView message;

    private static Map<String, Lang> cache = TranslateActivity.langs;
    private List<Translate> translates = null;

    View findViewById(int id) {
        return getView().findViewById(id);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.history, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        recyclerView = (RecyclerView) findViewById(R.id.list_history);

        if (cache.size() < 50) {
            AllLangsLoader allLangsLoader = new AllLangsLoader(getActivity());//if languages didn't load
            getActivity().getSupportLoaderManager().restartLoader(GET_ALL_LANGS, null, new CallBack<>(new SaveDataMap(), allLangsLoader, GET_ALL_LANGS));
        }

        message = (TextView) findViewById(R.id.message);
        update();
        Log.d(TAG, "onCreate: finish");
    }


    private class SaveDataMap implements ISaveData<Map<String, Lang>> {
        @Override
        public void saveData(Map<String, Lang> data, int id) {//on load languages
            cache = data;
            getActivity().getSupportLoaderManager().destroyLoader(id);
            loaderFinish();
        }
    }

    private class SaveDataList implements ISaveData<List<Translate>> {
        @Override
        public void saveData(List<Translate> data, int id) {//on load translates
            translates = data;
            getActivity().getSupportLoaderManager().destroyLoader(id);
            loaderFinish();
        }
    }

    private class EndUpdate implements ISaveData<Void> {
        @Override
        public void saveData(Void data, int id) {//on finish write changes of translate
            getActivity().getSupportLoaderManager().destroyLoader(id);
            if (getArguments() != null && getArguments().containsKey(TYPE)) {//update other fragment
                int position = getArguments().getInt(TYPE) == HistoryActivity.FRAGMENT_FAVORITE ? HistoryActivity.FRAGMENT_HISTORY : HistoryActivity.FRAGMENT_FAVORITE;
                ((HistoryActivity) getActivity()).updateFragment(position);
            }
        }
    }

    public void update() {
        recyclerView.setVisibility(View.GONE);
        message.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        if (getArguments() != null && getArguments().containsKey(TYPE)) {
            if (getArguments().getInt(TYPE) == HistoryActivity.FRAGMENT_FAVORITE) {//if show favorite translations
                translates = null;
                FavouriteTranslatesLoader favouriteTranslatesLoader = new FavouriteTranslatesLoader(getContext());//load from database favorite translations
                getActivity().getSupportLoaderManager().restartLoader(GET_FAVOURITE_TRANSLATES_LOADER, null, new CallBack<>(new SaveDataList(), favouriteTranslatesLoader, GET_FAVOURITE_TRANSLATES_LOADER));
                message.setText(getActivity().getResources().getText(R.string.empty_favorite));
            } else {
                translates = TranslateActivity.translates;//if show all translations
                message.setText(getActivity().getResources().getText(R.string.empty_history));
            }
        }
        loaderFinish();
    }

    private void loaderFinish() {//if all loaders finished
        if (cache.size() > 50 && translates != null) {
            progressBar.setVisibility(View.GONE);
            if (translates.size() > 0) {//if list of translates isn't empty
                recyclerView.setVisibility(View.VISIBLE);
                message.setVisibility(View.GONE);
                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                SimpleRecycleAdapter simpleRecycleAdapter = new SimpleRecycleAdapter(getActivity());
                recyclerView.setAdapter(simpleRecycleAdapter);
            } else {
                recyclerView.setVisibility(View.GONE);//if list of translates empty
                message.setVisibility(View.VISIBLE);
            }
        }
    }

    private class MyOnclickListener implements View.OnClickListener {

        int position;

        MyOnclickListener(int position) {
            this.position = position;//remember position of translate in list
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(getActivity(), TranslateActivity.class);
            if (TranslateActivity.translates.get(position).equals(translates.get(position)))
                intent.putExtra(TranslateActivity.POSITION, position);//send a number of position in list of translates
            else {
                intent.putExtra(TranslateActivity.POSITION, TranslateActivity.translates.indexOf(translates.get(position)));
            }
            startActivity(intent);
            getActivity().finish();
        }
    }

    private class MyOnTouchListener implements View.OnTouchListener {

        final Translate translate;

        MyOnTouchListener(Translate translate) {
            this.translate = translate;
        }

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            RatingBar ratingBar = (RatingBar) view;
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {//on click on favorite
                translate.setFavourite(1 - translate.getFavourite());
                ratingBar.setRating(translate.getFavourite());
                UpdateTranslateLoader loader = new UpdateTranslateLoader(getActivity(), translate);
                getActivity().getSupportLoaderManager().restartLoader(UPDATE_TRANSLATE, null, new CallBack<>(new EndUpdate(), loader, UPDATE_TRANSLATE));
                return true;
            }
            return false;
        }
    }

    private class SimpleRecycleAdapter extends RecyclerView.Adapter<SimpleRecycleAdapter.ViewHolder> {

        private final LayoutInflater li;

        private SimpleRecycleAdapter(Context context) {
            li = LayoutInflater.from(context);
            Log.d(TAG, "SimpleRecycleAdapter: size=" + translates.size());
            setHasStableIds(true);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(li.inflate(R.layout.translate_item_list, parent, false));
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            Log.d(TAG, "onBindViewHolder: position=" + position);
            Translate translate = translates.get(getItemCount() - position - 1);//load translate and show it
            holder.langs.setText(translate.getFirstCode().toUpperCase() + " - " + translate.getSecondCode().toUpperCase());
            holder.input.setText(translate.getInput());
            holder.output.setText(translate.getOutput());
            holder.btn.setOnClickListener(new MyOnclickListener(getItemCount() - position - 1));
            holder.favorite.setOnTouchListener(new MyOnTouchListener(translate));
            holder.favorite.setRating(translate.getFavourite());
        }

        @Override
        public long getItemId(int position) {
            return translates.get(position).hashCode();
        }

        @Override
        public int getItemCount() {
            return translates.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            final LinearLayout btn;
            final TextView input, output;
            final TextView langs;
            final RatingBar favorite;

            ViewHolder(View itemView) {
                super(itemView);
                btn = (LinearLayout) itemView.findViewById(R.id.translate);
                input = (TextView) itemView.findViewById(R.id.inputText);
                output = (TextView) itemView.findViewById(R.id.outputText);
                langs = (TextView) itemView.findViewById(R.id.langs);
                favorite = (RatingBar) itemView.findViewById(R.id.favourite);
            }
        }
    }
}
