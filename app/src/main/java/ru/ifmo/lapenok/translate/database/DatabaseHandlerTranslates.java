package ru.ifmo.lapenok.translate.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Алексей on 25.03.2017.
 * Database to contain translations
 *
 * @see Translate
 */
public class DatabaseHandlerTranslates extends SQLiteOpenHelper {

    private static final String TAG = "DatabaseTranslates";

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "translates";

    private static final String TABLE_TRANSLATE = "translate";
    private static final String KEY_ID = "_id";
    private static final String KEY_CODE_FIRST = "first";
    private static final String KEY_CODE_SECOND = "second";
    private static final String KEY_INPUT = "input";
    private static final String KEY_OUTPUT = "output";
    private static final String KEY_FAVORITE = "favorite";

    /**
     * create database
     *
     * @param context
     */
    public DatabaseHandlerTranslates(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * create tables in sqLiteDatabase
     *
     * @param sqLiteDatabase database
     */
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String createTranslateTable = "CREATE TABLE " + TABLE_TRANSLATE + " (" +
                KEY_ID + " INTEGER PRIMARY KEY, " +
                KEY_CODE_FIRST + " TEXT, " +
                KEY_CODE_SECOND + " TEXT, " +
                KEY_INPUT + " TEXT, " +
                KEY_OUTPUT + " TEXT, " +
                KEY_FAVORITE + " INTEGER " +
                ")";
        sqLiteDatabase.execSQL(createTranslateTable);
        Log.d(TAG, "onCreate: tables was created");
    }

    /**
     * update tables in sqLiteDatabase
     *
     * @param sqLiteDatabase database
     * @param i              old version of database handler
     * @param i1             new version of database handler
     */
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_TRANSLATE);
        Log.d(TAG, "onUpgrade: tables was deleted");
        onCreate(sqLiteDatabase);
    }

    private List<Translate> getTranslations(String query) {
        List<Translate> translates = new ArrayList<>();
        SQLiteDatabase db = null;
        Cursor cursor = null;
        try {
            db = this.getReadableDatabase();
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    translates.add(new Translate(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getInt(5)));
                }
                while (cursor.moveToNext());
            }
            return translates;
        } catch (Exception e) {
            Log.e(TAG, "getAllTranslations: " + e.getMessage(), e);
        } finally {
            if (cursor != null)
                cursor.close();
            if (db != null)
                db.close();
        }
        return translates;
    }

    /**
     * load from database all translations
     *
     * @return list of translations
     * @see Translate
     * @see List
     */
    public List<Translate> getAllTranslations() {
        String query = "SELECT " + KEY_ID + ", " + KEY_CODE_FIRST + ", " + KEY_CODE_SECOND + ", " + KEY_INPUT + ", " + KEY_OUTPUT + ", " + KEY_FAVORITE + " FROM " + TABLE_TRANSLATE;
        return getTranslations(query);
    }

    /**
     * insert into database data
     *
     * @param data data
     */
    public void add(Translate data) {
        SQLiteDatabase db = null;
        try {
            db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(KEY_CODE_FIRST, data.getFirstCode());
            values.put(KEY_CODE_SECOND, data.getSecondCode());
            values.put(KEY_INPUT, data.getInput());
            values.put(KEY_OUTPUT, data.getOutput());
            values.put(KEY_FAVORITE, data.getFavourite());
            long id = db.insertOrThrow(TABLE_TRANSLATE, null, values);
            data.setId((int) id);
        } catch (Exception e) {
            Log.e(TAG, "add: " + e.getMessage(), e);
        } finally {
            if (db != null)
                db.close();
        }
    }

    private Translate getTranslate(String query) {
        SQLiteDatabase db = null;
        Cursor cursor = null;
        try {
            db = this.getReadableDatabase();
            cursor = db.rawQuery(query, null);
            if (cursor != null && cursor.moveToFirst()) {
                Translate translate = new Translate(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getInt(5));
                cursor.close();
                return translate;
            }
        } catch (Exception e) {
            Log.e(TAG, "findTranslate: " + e.getMessage(), e);
        } finally {
            if (cursor != null)
                cursor.close();
            if (db != null)
                db.close();
        }
        return null;
    }

    /**
     * update column `favorite` in row in database
     *
     * @param translate row from database
     */
    public void updateTranslate(Translate translate) {
        SQLiteDatabase db = null;
        try {
            db = this.getWritableDatabase();
            String query = "UPDATE " + TABLE_TRANSLATE + " SET " + KEY_FAVORITE + "=" + translate.getFavourite() + " WHERE " + KEY_ID + "=" + translate.getId();
            db.execSQL(query);
            Log.d(TAG, "updateTranslate: updated");
        } catch (Exception e) {
            Log.e(TAG, "updateTranslate: " + e.getMessage(), e);
        } finally {
            if (db != null)
                db.close();
        }
    }

    /**
     * find translate by input string and two languages
     *
     * @param input      input string
     * @param firstCode  code of input string
     * @param secondCode code of result string
     * @return translate
     * @see Translate
     */
    public Translate findTranslate(String input, String firstCode, String secondCode) {
        String query = "SELECT " + KEY_ID + ", " + KEY_CODE_FIRST + ", " + KEY_CODE_SECOND + ", " + KEY_INPUT + ", " + KEY_OUTPUT + ", " + KEY_FAVORITE + " FROM " + TABLE_TRANSLATE + " WHERE " + KEY_CODE_FIRST + " like '%" + firstCode + "%' AND " + KEY_CODE_SECOND + " like '%" + secondCode + "%' AND " + KEY_INPUT + " like '%" + input + "%'";
        return getTranslate(query);
    }

    /**
     * find translate by input and second language
     *
     * @param input      input string
     * @param secondCode code of result string
     * @return translate
     * @see Translate
     */
    public Translate findTranslate(String input, String secondCode) {
        String query = "SELECT " + KEY_ID + ", " + KEY_CODE_FIRST + ", " + KEY_CODE_SECOND + ", " + KEY_INPUT + ", " + KEY_OUTPUT + ", " + KEY_FAVORITE + " FROM " + TABLE_TRANSLATE + " WHERE " + KEY_CODE_SECOND + " like '%" + secondCode + "%' AND " + KEY_INPUT + " like '%" + input + "%'";
        return getTranslate(query);
    }

    private void deleteAll(String table) {
        SQLiteDatabase db = null;
        try {
            db = this.getWritableDatabase();
            db.delete(table, null, null);
        } catch (Exception e) {
            Log.e(TAG, "deleteAllLangs: " + e.getMessage(), e);
        } finally {
            if (db != null)
                db.close();
        }
    }

    public void deleteAllTranslates() {
        deleteAll(TABLE_TRANSLATE);
    }
}
