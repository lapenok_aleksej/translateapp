package ru.ifmo.lapenok.translate.application;

import android.app.Application;
import android.content.Context;

/**
 * Created by Алексей on 15.04.2017.
 */

public class TranslateApplication extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
    }

    public static Context getContext() {
        return context;
    }
}
