package ru.ifmo.lapenok.translate.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import ru.ifmo.lapenok.translate.activity.TranslateActivity;
import ru.ifmo.lapenok.translate.database.DatabaseHandlerTranslates;

/**
 * Created by Алексей on 07.04.2017.
 *
 * @see AsyncTaskLoader
 */

public class DeleteAllTranslatesLoader extends AsyncTaskLoader<Void> {

    private static final String TAG = "DeleteAllTranslates";

    private final DatabaseHandlerTranslates db;

    public DeleteAllTranslatesLoader(Context context) {
        super(context);
        this.db = TranslateActivity.dbTranslates;
    }

    /**
     * delete all translates from database
     */
    @Override
    public Void loadInBackground() {
        db.deleteAllTranslates();
        return null;
    }

    @Override
    public void onStartLoading() {
        forceLoad();
    }
}
