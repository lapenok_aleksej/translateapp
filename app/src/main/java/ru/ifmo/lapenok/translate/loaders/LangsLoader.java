package ru.ifmo.lapenok.translate.loaders;

import android.content.Context;
import android.support.annotation.UiThread;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ru.ifmo.lapenok.translate.activity.TranslateActivity;
import ru.ifmo.lapenok.translate.database.DatabaseHandlerLangs;
import ru.ifmo.lapenok.translate.database.Lang;
import ru.ifmo.lapenok.translate.utils.DownloadUtils;
import ru.ifmo.lapenok.translate.utils.FileUtils;
import ru.ifmo.lapenok.translate.utils.JsonReader;

/**
 * Created by Алексей on 20.03.2017.
 *
 * @see AsyncTaskLoader
 */

public class LangsLoader extends AsyncTaskLoader<List<Lang>> {

    private static final String TAG = "LangsLoader";

    private final Context context;
    private final DatabaseHandlerLangs db;

    public LangsLoader(Context context) {
        super(context);
        this.context = context;
        if (TranslateActivity.dbLangs == null) {
            this.db = new DatabaseHandlerLangs(context);
        } else
            this.db = TranslateActivity.dbLangs;
    }

    /**
     * load from internet all languages and write into database
     *
     * @return list of languages
     * @see List
     * @see Lang
     */
    @Override
    public List<Lang> loadInBackground() {
        List<Lang> langs = db.getAllLangs();
        if (langs.size() > 50) {
            return langs;
        }
        db.deleteAllLangs();
        try {
            final File tmpFile = FileUtils.createTempExternalFile(context, "langs", "jz");
            DownloadUtils.downloadJsonLangs("ru", tmpFile);
            Object result = JsonReader.parse(new FileInputStream(tmpFile)).get("langs");
            if (result instanceof List<?>) {
                List result1 = (List) result;
                if (result1.size() != 0 && result1.get(0) instanceof Lang) {
                    @SuppressWarnings("unchecked")
                    List<Lang> back = (List<Lang>) result1;
                    Collections.sort(back, new Comparator<Lang>() {
                        @Override
                        public int compare(Lang lang, Lang t1) {
                            return lang.getName().compareTo(t1.getName());
                        }
                    });
                    db.add(back);
                    return back;
                } else
                    throw new AssertionError("langs is not a List of Lang");
            } else
                throw new AssertionError("langs is not a List");
        } catch (IOException e) {
            Log.e(TAG, "loadInBackground: IOException", e);
        }
        return null;
    }

    @Override
    @UiThread
    protected void onStartLoading() {
        forceLoad();
    }
}
