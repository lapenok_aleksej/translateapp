package ru.ifmo.lapenok.translate.loaders.callback;

/**
 * Created by Алексей on 27.03.2017.
 */
public interface ISaveData<T> {
    /**
     * calls when loader finish with result
     *
     * @param data result of loader
     * @param id   id of loader
     */
    public void saveData(T data, int id);
}
