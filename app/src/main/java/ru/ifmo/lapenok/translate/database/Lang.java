package ru.ifmo.lapenok.translate.database;

/**
 * Created by Алексей on 20.03.2017.
 */

public class Lang { //class to contains language

    private final String name;
    private final String code;

    /**
     * create new language
     *
     * @param name name of language
     * @param code code of language
     */
    public Lang(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }
}
