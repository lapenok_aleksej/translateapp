package ru.ifmo.lapenok.translate.loaders;

import android.content.Context;
import android.support.annotation.UiThread;
import android.support.v4.content.AsyncTaskLoader;

import ru.ifmo.lapenok.translate.database.DatabaseHandlerTranslates;
import ru.ifmo.lapenok.translate.database.Translate;

/**
 * Created by Алексей on 02.04.2017.
 */

public class UpdateTranslateLoader extends AsyncTaskLoader<Void> {
    private final DatabaseHandlerTranslates db;
    private final Translate translate;

    /**
     * @param context   current context
     * @param translate argument to update
     */
    public UpdateTranslateLoader(Context context, Translate translate) {
        super(context);
        db = new DatabaseHandlerTranslates(context);
        this.translate = translate;
    }

    /**
     * update translate in database
     */
    @Override
    public Void loadInBackground() {
        db.updateTranslate(translate);
        return null;
    }

    @Override
    @UiThread
    protected void onStartLoading() {
        forceLoad();
    }
}
